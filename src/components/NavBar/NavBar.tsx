import React from 'react';
import './styles.scss';
import {IconLamodaLogo, IconTrash} from "../../assets/icons";


function NavBar() {

    return (
        <div className="navbar">
            <div>
                <span className="navbar__nav">Женщинам</span>
                <span className="navbar__nav">Мужчинам</span>
                <span className="navbar__nav">Детям</span>
            </div>

            <div className="navbar__logo">
                <IconLamodaLogo/>
            </div>
            <div className="navbar__right-items">
                <button className="navbar__login">Войти</button>
                <button className="navbar__trash">
                    <IconTrash/>
                    Корзина
                </button>
            </div>
        </div>
    )
}


export default NavBar;
