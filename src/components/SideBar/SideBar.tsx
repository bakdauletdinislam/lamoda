import React from 'react';
import './styles.scss';
import Accordion from "../Accardion/Accardion";

function SideBar() {

    const accordionItems = [
        {
            title: 'Обувь',
            content: (
                <div>
                    <div className="sidebar-item">
                        <p className="sidebar-item__product">Ботинки</p>
                        <p className="sidebar-item__count">6572</p>
                    </div>
                    <div className="sidebar-item">
                        <p className="sidebar-item__product">Сандали</p>
                        <p className="sidebar-item__count">8454</p>
                    </div>
                    <div className="sidebar-item">
                        <p className="sidebar-item__product">Сапоги</p>
                        <p className="sidebar-item__count">2457</p>
                    </div>
                    <div className="sidebar-item">
                        <p className="sidebar-item__product">Туфли</p>
                        <p className="sidebar-item__count">7658</p>
                    </div>
                    <div className="sidebar-item">
                        <p className="sidebar-item__product">Домашняя</p>
                        <p className="sidebar-item__count">5243</p>
                    </div>
                    <div className="sidebar-item">
                        <p className="sidebar-item__product">Кроссовки и кеды</p>
                        <p className="sidebar-item__count">876</p>
                    </div>
                </div>
            ),
        },
        {
            title: 'Одежда',
            content: (
                <div>
                    <div className="sidebar-item">
                        <p className="sidebar-item__product">Брюки</p>
                        <p className="sidebar-item__count">5643</p>
                    </div>
                    <div className="sidebar-item">
                        <p className="sidebar-item__product">Верхняя одежда</p>
                        <p className="sidebar-item__count">1234</p>
                    </div>
                    <div className="sidebar-item">
                        <p className="sidebar-item__product">Джинсы</p>
                        <p className="sidebar-item__count">5476</p>
                    </div>
                    <div className="sidebar-item">
                        <p className="sidebar-item__product">Домашняя одежда</p>
                        <p className="sidebar-item__count">6334</p>
                    </div>
                    <div className="sidebar-item">
                        <p className="sidebar-item__product">Майки</p>
                        <p className="sidebar-item__count">234</p>
                    </div>
                    <div className="sidebar-item">
                        <p className="sidebar-item__product">Нижнее белье</p>
                        <p className="sidebar-item__count">765</p>
                    </div>
                </div>
            ),
        },
        {
            title: 'Аксессуары',
            content: (
                <div>
                    <div className="sidebar-item">
                        <p className="sidebar-item__product">Галстуки</p>
                        <p className="sidebar-item__count">43212</p>
                    </div>
                    <div className="sidebar-item">
                        <p className="sidebar-item__product">Зонты</p>
                        <p className="sidebar-item__count">253</p>
                    </div>
                    <div className="sidebar-item">
                        <p className="sidebar-item__product">Очки</p>
                        <p className="sidebar-item__count">12354</p>
                    </div>
                    <div className="sidebar-item">
                        <p className="sidebar-item__product">Перчатки</p>
                        <p className="sidebar-item__count">65445</p>
                    </div>
                    <div className="sidebar-item">
                        <p className="sidebar-item__product">Рюкзаки</p>
                        <p className="sidebar-item__count">324</p>
                    </div>
                </div>
            ),
        },
        {
            title: 'Красота',
            content: (
                <div>
                    <div className="sidebar-item">
                        <p className="sidebar-item__product">Антисептик</p>
                        <p className="sidebar-item__count">122</p>
                    </div>
                    <div className="sidebar-item">
                        <p className="sidebar-item__product">Волосы</p>
                        <p className="sidebar-item__count">4356</p>
                    </div>
                    <div className="sidebar-item">
                        <p className="sidebar-item__product">Лицо</p>
                        <p className="sidebar-item__count">4232</p>
                    </div>
                    <div className="sidebar-item">
                        <p className="sidebar-item__product">Тело</p>
                        <p className="sidebar-item__count">213</p>
                    </div>
                    <div className="sidebar-item">
                        <p className="sidebar-item__product">Наборы</p>
                        <p className="sidebar-item__count">8021</p>
                    </div>
                </div>
            ),
        },
        {
            title: 'Спорт',
            content: (
                <div>
                    <div className="sidebar-item">
                        <p className="sidebar-item__product">Баскетбол</p>
                        <p className="sidebar-item__count">12300</p>
                    </div>
                    <div className="sidebar-item">
                        <p className="sidebar-item__product">Бег</p>
                        <p className="sidebar-item__count">546</p>
                    </div>
                    <div className="sidebar-item">
                        <p className="sidebar-item__product">Волейбол</p>
                        <p className="sidebar-item__count">76854</p>
                    </div>
                    <div className="sidebar-item">
                        <p className="sidebar-item__product">Плавание</p>
                        <p className="sidebar-item__count">3245</p>
                    </div>
                    <div className="sidebar-item">
                        <p className="sidebar-item__product">Теннис</p>
                        <p className="sidebar-item__count">76562</p>
                    </div>
                </div>
            ),
        },
    ]

    return (
        <div className="sidebar__menu">
            <Accordion items={accordionItems}/>
        </div>
    )
}


export default SideBar;
