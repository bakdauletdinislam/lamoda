import {ReactNode, useState} from 'react';
import AccordionItem from "./component/AccardionItem";
import './styles.scss';

export interface AccordionData {
    title: string;
    content: ReactNode;
}

function Accordion({items}: { items: Array<AccordionData> }) {
    const [currentIdx, setCurrentIdx] = useState(-1);
    const onClickArrow = (idx: number) => {
        setCurrentIdx(idx);
    };

    return (
        <ul className="accordion">
            {items.map((item, idx) => (
                <AccordionItem
                    key={idx}
                    data={item}
                    isOpen={idx === currentIdx}
                    onClick={() => onClickArrow(idx)}
                />
            ))}
        </ul>
    );
}

export default Accordion;
