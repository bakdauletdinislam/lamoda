import {RefObject, useEffect, useRef, useState} from 'react';
import {AccordionData} from "../Accardion";
import '../styles.scss';

export function getRefValue<C>(ref: RefObject<C>) {
    return ref.current as C;
}

function AccordionItem({
                           data,
                           isOpen,
                           onClick,
                       }: {
    data: AccordionData;
    isOpen: boolean;
    onClick: () => void;
}) {
    const contentRef = useRef<HTMLDivElement>(null);
    const [height, setHeight] = useState(0);

    useEffect(() => {
        if (isOpen) {
            const contentEl = getRefValue(contentRef);

            setHeight(contentEl.scrollHeight);
        } else {
            setHeight(0);
        }
    }, [isOpen]);

    return (
        <li className={`accordion-item ${isOpen ? 'active' : ''}`}>
            <h2 className="accordion-item-title">
                <button className="accordion-item-btn" onClick={onClick}>
                    {data.title}
                </button>
            </h2>
            <div className="accordion-item-container" style={{height}}>
                <div ref={contentRef} className="accordion-item-content">
                    {data.content}
                </div>
            </div>
        </li>
    );
}

export default AccordionItem;
