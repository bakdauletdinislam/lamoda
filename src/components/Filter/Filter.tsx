import React, {ReactNode, useState} from 'react';
import './styles.scss';


interface FilterProps {
    name?: string;
    content?: ReactNode;
    type?: 'checkbox' | 'radio' | 'slider',


}

const Filter: React.FC<FilterProps> = () => {
    const [open, setOpen] = useState<boolean>(false);

    return (
        <button className="filter">Filter</button>
    )

}


export default Filter;
