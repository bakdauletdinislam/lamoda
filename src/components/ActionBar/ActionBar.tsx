import React from 'react';
import './styles.scss';

function ActionBar() {

    const arrayActions = [
        {
            title: 'Новинки'
        },
        {
            title: 'Одежда'
        },
        {
            title: 'Обувь'
        },
        {
            title: 'Аксессуары'
        },
        {
            title: 'Бренды'
        },
        {
            title: 'Premium'
        }
    ]


    return (
        <div className="actionbar">
            <div className="actionbar__actions">
                {arrayActions.map((action, actionIndex) => (
                    <button key={actionIndex} className="actionbar__title">{action.title}</button>
                ))}
            </div>
            <input type="text" title="Search" className="actionbar__search"/>
        </div>
    )
}

export default ActionBar;
