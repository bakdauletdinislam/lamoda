import React from 'react';
import './styles.scss';


const Footer = () => {

    return (
        <div className="footer">
            <div className="footer-send">
                <p className="footer-send__header">Cкидка 10% за подписку <br/>на наши новости</p>
                <div className="footer-send__form">
                    <input type="text" placeholder="Введите свой email"/>
                    <button>Для нее</button>
                    <button>Для него</button>
                </div>
                <p className="footer-send__terms">Условия акции</p>
            </div>
            <div className="footer-info">
                <div className="help-container">
                    <span className="help-container__title">Помощь</span>
                    <ul>
                        <li className="help-container__text">Статус заказа</li>
                        <li className="help-container__text">Центр поддержки</li>
                        <li className="help-container__text">Как оформить заказ</li>
                        <li className="help-container__text">Как выбрать размер</li>
                        <li className="help-container__text">Условия доставки</li>
                        <li className="help-container__text">Мои заказы</li>
                        <li className="help-container__text">Возврат</li>
                        <li className="help-container__text">Публичная офорте</li>
                    </ul>
                </div>
                <div className="about-container">
                    <span className="about-container__title">О нас</span>
                    <div className="about-container__icons">
                        <div className="about-container__icon"/>
                        <div className="about-container__icon"/>
                        <div className="about-container__icon"/>
                        <div className="about-container__icon"/>
                    </div>
                    <a className="about-container__text">Вакансии</a>
                    <a className="about-container__text">Подключиться к партнёрской <br/>программе
                        "Маркетплейс"</a>
                </div>
                <div className="payment-container">
                    <span className="payment-container__title">Способы оплаты</span>
                    <div className="payment-container__variants variant">
                        <div className="variant__mastercard"/>
                        <div className="variant__visa"/>
                    </div>
                    <span className="payment-container__text">Вы можете оплатить покупки<br/> наличными при получении, либо<br/> выбрать <a
                        className="payment-container__text--link"> другой способ оплаты</a></span>

                </div>
                <div className="download-container">
                    <span className="download-container__title">Мода в кармане</span>
                    <div className="download-container__market"/>
                    <div className="download-container__market"/>
                    <div className="download-container__market"/>

                    <span className="download-container__text">Вы также можете перейти на <br/> <a
                        className="download-container__link-text">мобильную версию сайта</a></span>

                </div>
            </div>
        </div>
    )
}

export default Footer;
