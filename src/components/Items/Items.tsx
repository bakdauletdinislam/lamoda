import React, {FunctionComponent, SVGProps} from 'react';
import './styles.scss';

interface ItemsProps {
    icon: FunctionComponent<SVGProps<SVGSVGElement>>;
    price: string;
    brand: string;
    model: string;
}

function Items() {

    const content = [
        {
            icon: '',
            price: '54000',
            brand: 'Reebok',
            model: 'Кроссовки Royal Ultra'
        },
        {
            icon: '',
            price: '50000',
            brand: 'Nike',
            model: 'Кроссовки Nike'
        },
        {
            icon: '',
            price: '55000',
            brand: 'Fila',
            model: 'Кроссовки Fila'
        },
        {
            icon: '',
            price: '18000',
            brand: 'Adidas',
            model: 'Кроссовки Adidas'
        },
        {
            icon: '',
            price: '54000',
            brand: 'Reebok',
            model: 'Кроссовки Reebok'
        },

        {
            icon: '',
            price: '18000',
            brand: 'Adidas',
            model: 'Кроссовки Adidas'
        },
        {
            icon: '',
            price: '54000',
            brand: 'Reebok',
            model: 'Кроссовки Reebok'
        },
        {
            icon: '',
            price: '18000',
            brand: 'Adidas',
            model: 'Кроссовки Adidas'
        },
        {
            icon: '',
            price: '54000',
            brand: 'Reebok',
            model: 'Кроссовки Reebok'
        },
        {
            icon: '',
            price: '18000',
            brand: 'Adidas',
            model: 'Кроссовки Adidas'
        },
        {
            icon: '',
            price: '54000',
            brand: 'Reebok',
            model: 'Кроссовки Reebok'
        },
        {
            icon: '',
            price: '54000',
            brand: 'Reebok',
            model: 'Кроссовки Reebok'
        },
        {
            icon: '',
            price: '54000',
            brand: 'Reebok',
            model: 'Кроссовки Reebok'
        },
        {
            icon: '',
            price: '18000',
            brand: 'Adidas',
            model: 'Кроссовки Adidas'
        },
        {
            icon: '',
            price: '54000',
            brand: 'Reebok',
            model: 'Кроссовки Reebok'
        },
        {
            icon: '',
            price: '54000',
            brand: 'Reebok',
            model: 'Кроссовки Reebok'
        },
    ];

    return (
        <>
            <div className="products">
                {content.map((item, itemIndex) => (
                    <div className="products__item product" key={itemIndex}>
                        <div className="product__icon">{item.icon}</div>
                        <div className="product__price">{item.price}</div>
                        <div className="product__brand">{item.brand}</div>
                        <div className="product__model">{item.model}</div>
                    </div>
                ))}
            </div>
        </>
    )

}

export default Items;
