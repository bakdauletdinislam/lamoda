import React from 'react';
import './styles.scss';
import {IconHanger, IconMoneyCard, IconPaper} from "../../assets/icons";

function Header() {
    return (
        <div className="header">
            <div className="header__items">
                <button className="header__item">
                    <IconHanger/>
                    <span className="header__text">Доставка с примеркой</span>
                </button>
                <button className="header__item">
                    <IconPaper/>
                    <span className="header__text">Подлинные товары</span>
                </button>
                <button className="header__item--money">
                    <IconMoneyCard/>
                    <span className="header__text">Платите когда хотите</span>
                </button>
            </div>

        </div>
    )
}

export default Header;
