import React from 'react';
import Header from "../../components/Header";
import NavBar from "../../components/NavBar";
import ActionBar from "../../components/ActionBar/ActionBar";
import './styles.scss';
import SideBar from "../../components/SideBar/SideBar";
import Items from "../../components/Items";
import Filter from "../../components/Filter";
import Footer from "../../components/Footer";


function HomePage() {

    return (
        <div className="layout">
            <div className="layout__header">
                <Header/>
                <NavBar/>
                <ActionBar/>
            </div>
            <div className="content">
                <p className="content__title">Мужская одежда</p>
                <p className="content__count">21125 товаров</p>
            </div>
            <div className="layout__wrapper">
                <div className="sidebar">
                    <SideBar/>
                </div>
                <div className="items">
                    <Filter/>
                    <Items/>
                </div>
            </div>
            <div className="layout__footer">
                <Footer/>
            </div>
        </div>
    )
}

export default HomePage;
