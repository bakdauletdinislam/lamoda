export {ReactComponent as IconHanger} from './hanger.svg';
export {ReactComponent as IconPaper} from './paper.svg';
export {ReactComponent as IconMoneyCard} from './money-card.svg';
export {ReactComponent as IconLamodaLogo} from './lamoda.svg';
export {ReactComponent as IconTrash} from './trash.svg';
